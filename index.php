<?php
//------------------------------------------ deklaracija polja
$ocjene = array('nedovoljan', 'dovoljan', 'dobar', 'vrlo dobar', 'odličan');
ispis($ocjene);

//zamjena zadnje ocjene
$ocjene[4] = "izvrstan";
ispis($ocjene);

$predmeti = array(
    'mikroračunala', 'web 2', 'operacijski sustavi', 'osnove programskog jezika JAVA',
    'C#', 'programsko inženjerstvo', 'teng 4'
);
ispis($predmeti);

$godina = array(' ', '31', '28', '31', '30', '31', '30', '31', '31', '30', '31', '30', '31');
unset($godina[0]);
ispis($godina);

$prijestupna = array(' ', '31', '29', '31', '30', '31', '30', '31', '31', '30', '31', '30', '31');;
//-------------------------------------------


//zamjena mjesta predmeta
$predmeti_tmp = array();
$predmeti_tmp[0] = $predmeti[1];
$predmeti[1] = $predmeti[5];
$predmeti[5] = $predmeti_tmp[0];
ispis($predmeti);

//polje za zamjenu pozicija
$pozicije = array();
$pozicije = [3, 5];
pozicije($predmeti, $pozicije);


//------------------------------------------------funkcije
//ispis polja
function ispis($polje)
{
    echo "<pre>";
    print_r($polje);
    echo "</pre>";
}

//zamjena pozicija
function pozicije($predmeti, $pozicije)
{
    $predmeti_tmp = array();
    $predmeti_tmp[0] = $predmeti[$pozicije[0]];
    $predmeti[$pozicije[0]] = $predmeti[$pozicije[1]];
    $predmeti[$pozicije[1]] = $predmeti_tmp[0];
    ispis($predmeti);
}

//generiranje tablice s predmetima i poziv
generateTable($predmeti);
function generateTable($polje)
{
    $tablica = "<table border =><tr><th>Redni broj</th> <th>Predmet</th></tr>";
    foreach ($polje as $key => $value) {
        $tablica .= "<tr><td>" . $key + 1 . "</td> <td>" . $value . "</td></tr>";
    }
    $tablica .= "</table>";

    echo $tablica, '<br>';
}

//provjera prijestupne godine i ispis god i mj
kalendar(2020, 2, $godina, $prijestupna);
function kalendar($god, $mj, $godina, $prijestupna)
{
    if ($god % 4 == 0) {
        echo "$god je prijestupna i $mj. mjesec ima $prijestupna[$mj] dana";
    } else {
        echo "$god nije prijestupna i $mj. mjesec ima $godina[$mj] dana";
    }
}
